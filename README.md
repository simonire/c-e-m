# C-E M

#### 介绍
1.  振华兄的C-E M直通板源代码
2.  对应PCB为C-E V1.4
3.  支持背光调整，支持关闭背光，支持外置供电
4.  FUSB302库与硬件模拟IIC库来自网络，经过修改后使用
5.  PCB工程已开源 https://oshwhub.com/infinite_possibilities/typec_to_edp_by-zhen-hua-xiong
6.  非商业目的自由使用，若商业使用需与我沟通

#### 开发环境

1.  Keil V5
2.  VS Code
3.  Keil Assistant

#### 使用说明

1.  编译后通过串口烧录

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 了解更多

1.  加入QQ群交流:942247457
2.  加我QQ号:2315518422
3.  加我微信:CatBoxz


