#ifndef _I2C_H_
#define _I2C_H_

/*********************************************************************
 * INCLUDES
 */
#include "gd32f1x0.h"
#include "delay.h"

/*********************************************************************
 * DEFINITIONS
 */
// I2C_SCL时钟
#define IIC_SCL_CLK         RCU_GPIOA                   // GPIO端口时钟
#define IIC_SCL_PORT        GPIOA                       // GPIO端口
#define IIC_SCL_PIN         GPIO_PIN_13                 // GPIO引脚
// I2C_SDA时钟
#define IIC_SDA_CLK         RCU_GPIOA                   // GPIO端口时钟
#define IIC_SDA_PORT        GPIOA                       // GPIO端口
#define IIC_SDA_PIN         GPIO_PIN_14                 // GPIO引脚

/*********************************************************************
 * MACROS
 */
#define IIC_SDA_READ()      gpio_input_bit_get(IIC_SDA_PORT, IIC_SDA_PIN) 

/*********************************************************************
 * API FUNCTIONS
 */
void IIC_Init(void);             
void IIC_Start(void);
void IIC_Stop(void);
void IIC_SendByte(uint8_t ucByte);
uint8_t IIC_ReadByte(bool ack);
uint8_t IIC_WaitAck(void);
void IIC_Ack(void);
void IIC_NAck(void);
uint8_t IIC_CheckDevice(uint8_t address);
bool IIC_WriteSlave(uint8_t slave_addr, uint8_t *data, uint8_t len);
bool IIC_ReadSlave(uint8_t slave_addr, uint8_t data, uint8_t *buf, uint32_t len);

#endif /* _I2C_H_ */
