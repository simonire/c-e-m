#include "usart.h"

void USART_Init(void){
    gd_eval_com_init(EVAL_COM1);
}

void USART_SendBytes(const char *msg){
    #ifdef SHOW_UART
    while(*msg){
        usart_data_transmit(EVAL_COM1, *(msg++));
        while(usart_flag_get(EVAL_COM1, USART_FLAG_TBE) == RESET);
    }
    #endif
}
